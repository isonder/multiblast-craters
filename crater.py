# -*- coding: utf-8 -*-
"""
:synopsis: Process crater morphology.

:moduleauthor: ingo
"""
# from builtins import print
import numpy as np
import matplotlib.pyplot as plt
from IPython.display import display
from scipy import interpolate as intp

pi = np.pi


class Profile(object):
    """Base profile object. Not for direct use. Provides some common parameters
    used by the specialized Profile classes.

    :param typ: Type descriptor of profile. One of 'parallel', 'orthogonal', \
    'elliptic'.
    :type typ: str
    """
    plotopts = {'pointchar': 'x', 'setlabels': True,
                'ylabel': 'z', 'xlabel': 'x',
                'xlabelpos-ratio': .25, 'ylabelpos-ratio': .8,
                'xlim': (-1.5, 1.5), 'ylim': (-.85, .25)}

    def __init__(self, typ: str) -> None:
        """See the class doc.
        :param typ: Type of the profile.
        """
        object.__init__(self)
        self.typ = typ
        self.subprofiles = [self]

    def set_offset(self, offset):
        print(":-o")

    @classmethod
    def set_plotlabels(cls, ax, kwds):
        """Create labels for the `stdplot` function."""
        if kwds['setlabels']:
            ax.set_xlabel("$%s / \mathrm{m}$" % kwds['xlabel'])
            ax.set_ylabel("$%s / \mathrm{m}$" % kwds['ylabel'])
            ax.set_yticks(np.arange(-1., .25, .25))
        try:
            label = kwds['label']
            ax.set_title(label, fontsize='medium')
        except KeyError:
            pass
        try:
            vollabel = kwds['vollabel']
            r = kwds['ylabelpos-ratio']
            xlim, ylim = ax.get_xlim(), ax.get_ylim()
            xloc = kwds['xlabelpos-ratio'] * xlim[1]
            yloc = r * ylim[1] + (1. - r) * ylim[0]
            try:
                if kwds['labelpos'] == 'bottom':
                    yloc = ylim[1] + ylim[0] - yloc
            except KeyError:
                pass
            ax.text(xloc, yloc,
                    "$\mathrm{volume:\ %s\,m^3}$" % vollabel,
                    fontsize='medium')
        except KeyError:
            pass


def sd_length(l, mode: str='none'):
    """Standard deviation of a measured length
    :rtype : ndarray or float
    :param l: length for which to calculate the sd.
    :param mode: Mode switch to distinguish higher errors for
     subsurface measurements.
    """
    l = np.abs(l)
    if mode == 'subsurf':
        sd = .025 * l * (1 + .6 * l / 1.25)
    else:
        sd = .025 * l * (1 + .6 * l / 2.50)
    return sd


class NullProfile(Profile):
    """...
    """
    radius = 0.
    horizon = 0.
    min_rim = np.array([0., 0.])
    max_rim = np.array([0., 0.])

    @staticmethod
    def dV(n: int=1000000) -> np.ndarray:
        """Compute the volume elements.
        :param n: Number of volume elements to compute.
        :return: Volume elements.
        :rtype : ndarray
        """
        return np.zeros(n, dtype=np.float)

    def __init__(self, typ: str) -> None:
        Profile.__init__(self, typ)


class CircularNullProfile(NullProfile):
    """...
    """
    center_depth = 0.
    hrz = np.array([-2., 0., 2.])
    vrt = np.array([0., 0., 0.])

    def __init__(self, typ: str) -> None:
        NullProfile.__init__(self, typ)


class CircularProfile(Profile):
    """Circular crater profile.

    :param typ: Type specifier.
    :type typ: str
    :param data: Profile data as tuple of x-y value arrays.
    :type data: tuple
    :param src: Path to data file.
    :type src: str
    :param centerindex: Index of center element of profile data. (will be \
    auto-determined if `None`.)
    :type centerindex: int
    :param padlevel: Don't know...
    :type padlevel: float
    :param extr: Type of extreme value at crater center. 'min' or 'max'. \
    Helps to find the center.
    :type extr: str
    """
    def __init__(self, typ="parallel", data=None, src=None,
                 centerindex=None, padlevel=0., extr='min'):
        Profile.__init__(self, typ)
        if src is not None:
            data = np.loadtxt(src, skiprows=1)
            # horizontal axis in meters
            self.hrz = data[:, 0] / 100.
            # vertical depth in meters (negative)
            self.vrt = -data[:, 1] / 100. - padlevel
        else:
            try:
                self.hrz, self.vrt = data[0].copy(), data[1].copy()
            except TypeError:
                raise TypeError("data not given.")
        # Indeces of crater rim
        self.idx_max_1, self.idx_max_2 = -10000, -10000
        self.rim_1 = np.array([np.nan, np.nan], dtype=float)
        self.rim_2 = np.array([np.nan, np.nan], dtype=float)
        self.center_depth = np.nan
        self.extr = extr
        self.local_extr = self.extr
        self.center_set = centerindex is not None
        if self.center_set:
            self.idx_center1 = centerindex
        self._getmin()
        self._interp()
        self.padlevel = padlevel
        self.offset = 0.
        self.color_depth = 'blue'
        self.color_z0 = 'blue'
        self._horizon = None
        self._sigma_horizon = None
        self._sigma_radius = None

    def _center_indexes(self):
        """Find center extremum and its index"""
        if not self.center_set:
            if self.extr == "min":
                self.idx_center1 = np.where(self.vrt == self.vrt.min())[0][0]
            else:
                self.idx_center1 = np.where(self.vrt == self.vrt.max())[0][0]
        self.idx_center2 = self.idx_center1
        while self.vrt[self.idx_center2 + 1] == self.vrt[self.idx_center1]:
            self.idx_center2 += 1

    def _get_rims(self):
        """Find maxima on both sides of minimum"""
        if self.extr == "min":
            self.rim_1[1] = self.vrt[:self.idx_center1].max()
            self.rim_2[1] = self.vrt[self.idx_center2:].max()
        else:
            self.rim_1[1] = self.vrt[:self.idx_center1].min()
            self.rim_2[1] = self.vrt[self.idx_center2:].min()
        self._get_rim_indeces()
        self.rim_1[0] = self.hrz[self.idx_max_1]
        self.rim_2[0] = self.hrz[self.idx_max_2]

    def _get_rim_indeces(self):
        self.idx_max_1 = np.where(self.vrt[:self.idx_center1]
                                  == self.rim_1[1])[0][-1]
        self.idx_max_2 = np.where(self.vrt[self.idx_center2:]
                                  == self.rim_2[1])[0][0] + self.idx_center2

    def _getmin(self):
        """Find minimum and set x-origin there. Find maxima."""
        self._center_indexes()
        self._get_rims()
        # move x-origin to minimum
        center = .5 * (self.hrz[self.idx_center2] + self.hrz[self.idx_center1])
        self.hrz -= center
        self.rim_1[0] -= center
        self.rim_2[0] -= center
        self.center_depth = self.vrt[self.idx_center1]

    def _interp(self):
        """Setup the interpolation function
        :rtype :
         :class:`scipy.interpolate.fitpack2.InterpolatedUnivariateSpline`
        """
        self.depth = intp.InterpolatedUnivariateSpline(self.hrz, self.vrt, k=1)

    def set_offset(self, offset: float) -> None:
        """Add *offset* to the vertical profile component."""
        self.vrt += offset
        self._getmin()
        self._interp()
        self.padlevel += offset

    def get_x(self, z, half=1) -> float:
        """Inverse interpolation around the crater rim.
        :rtype : float
        """
        if half == 1:
            start = np.where(self.rim_1[0] <= self.hrz)[0][0]
            end = self.idx_center1 + 1
            rim = self.rim_1
            if self.local_extr == "min":
                slope = -1
            else:
                slope = 1
            step = 1
        else:
            end = self.idx_center2 + 1
            start = np.where(self.rim_2[0] <= self.hrz)[0][0]
            rim = self.rim_2
            if self.local_extr == "min":
                slope = 1
            else:
                slope = -1
            step = -1
        if z == rim[1]:
            return rim[0]
        h, v = self.hrz[start:end:step], self.vrt[start:end:step]
        i, z2 = 1, v[0] - z
        z1 = z2
        while i < len(h):
            z1 = z2
            z2 = v[i] - z
            i += 1
            if z1 * z2 < 0. and slope * (z2 - z1) > 0:
                break
        if z1 == z2 or i == len(h):
            if half == 1:
                return self.rim_1[0]
            else:
                return self.rim_2[0]
        i -= 1
        ret = (h[i - 1] - h[i]) * (z - v[i - 1]) / (v[i - 1] - v[i]) + h[i - 1]
        if abs(ret) > abs(rim[0]):
            return rim[0]
        return ret

    @property
    def min_rim(self):
        """Vertical location of lower crater rim.

        :type: float
        """
        return min(self.rim_1[1], self.rim_2[1])

    @property
    def max_rim(self):
        """Vertical location of higher crater rim.

        :type : float
        """
        return max(self.rim_1[1], self.rim_2[1])

    @property
    def radius(self) -> float:
        """Averaged crater radius.

        :rtype : float
        """
        return .5 * (-self.rim_1[0] + self.rim_2[0])

    @property
    def sigma_radius(self) -> float:
        """
        :return: standard deviation of crater radius.
        :rtype: float
        """
        if self._sigma_radius is None:
            self._sigma_radius = sd_length(self.radius)
        return self._sigma_radius

    @property
    def horizon(self) -> float:
        """Pad level at r -> infinity.
        :rtype: float
        """
        return .5 * (self.vrt[0] + self.vrt[-1])

    @property
    def sigma_horizon(self) -> float:
        if self._sigma_horizon is None:
            self._sigma_horizon =\
                0.5 * np.sqrt(sd_length(self.vrt[0]) ** 2 +
                              sd_length(self.vrt[-1]) ** 2)
        return self._sigma_horizon

    def dV(self, n=1000000, z='min') -> np.ndarray:
        """Volume elements. The `z` switch is used in the Crater.minvol and
        Crater.maxvol property methods.

        :param n: number of elements to create/calculate.
        :type n: int
        :param z: One of 'min', 'max'.
        :type z: str
        :returns: Length-n array with the volume elements.
        :rtype: ndarray
        """
        ret = np.zeros(n)
        # left half
        if z == 'min':
            z0 = self.min_rim
        elif z == 'max':
            z0 = self.max_rim
        else:
            z0 = z
        xmax = self.get_x(z0, half=1)
        x = np.linspace(0, xmax, n + 1, True)
        x1 = x[:-1]
        x2 = x[1:]
        ret += 0.5 * pi * (x2 ** 2 - x1 ** 2) * (-self.depth(x1) + z0)
        # right half
        xmax = self.get_x(z0, half=2)
        x = np.linspace(0, xmax, n + 1, True)
        x1, x2 = x[:-1], x[1:]
        ret += 0.5 * pi * (x2 ** 2 - x1 ** 2) * (-self.depth(x1) + z0)
        return ret

    def stdplot(self, ax, kwds: dict=Profile.plotopts) -> None:
        """Creates a plot of the current crater state.

        :type kwds: dict
        :param ax: The axes object to plot into.
        :type ax: :class:`matplotlib.axes.AxesSubplot`
        """
        ax.plot(self.hrz, self.vrt, marker=kwds['pointchar'],
                color=self.color_depth)
        x = np.linspace(self.hrz[0], self.hrz[-1], 1000, True)
        ax.plot(x, self.depth(x),
                color=self.color_depth, label="$\mathrm{%s}$" % self.typ)
        ax.plot([self.get_x(self.min_rim, 1), self.get_x(self.min_rim, 2)],
                2 * [self.min_rim], color=self.color_z0, linestyle='dashed')
        ax.plot([self.get_x(self.max_rim, 1), self.get_x(self.max_rim, 2)],
                2 * [self.max_rim], color=self.color_z0, linestyle='dotted')
        ax.set_xlim(kwds['xlim'])
        ax.set_ylim(kwds['ylim'])
        if self.typ == 'parallel':
            kwds['xlabel'] = 'x'
        else:
            kwds['xlabel'] = 'y'
        try:
            if kwds['setlabels']:
                self.set_plotlabels(ax, kwds)
        except KeyError:
            pass
        ax.set_aspect(1.0)


class EllipticProfile(Profile):
    """Container for two perpendicular circular profiles. Calculates volumes
    of elliptically shaped volume elements.

    :keyword para: A parallel profile.
    :type para: :class:`crater.CircularProfile`
    :keyword orth: An orthogonal profile.
    :type orth: :class:`crater.CircularProfile`
    :keyword srcs: 2 paths to profile data sources. \
    `src[0] = parallel`.
    :type srcs: length 2 tuple(str).
    """
    def __init__(self, para: CircularProfile, orth: CircularProfile,
                 srcs: tuple=('', '')):
        Profile.__init__(self, typ='elliptic')
        try:
            assert isinstance(para, CircularProfile)
            assert isinstance(orth, CircularProfile)
            self.para = para  # parallel profile
            self.orth = orth  # orthogonal profile
        except AssertionError:
            try:
                assert isinstance(srcs, tuple)
                for src in srcs:
                    assert isinstance(src, str)
                    assert src != ''
                self.para = CircularProfile(src=srcs[0], typ='parallel')
                self.orth = CircularProfile(src=srcs[1], typ='orthogonal')
            except AssertionError:
                s = "EllipticProfile needs to be called either using two "
                s += ":class:`CircularProfile` instances (*para*, *orth*), "
                s += "or with the *srcs* tuple for the individual profile "
                s += "data sources."
                raise AssertionError(s)
        self._sigma_radius = None
        self.offset = self.para.padlevel - self.orth.padlevel
        if not self.offset == 0.:
            self.orth.set_offset(self.offset)
        # take the least depth (minimum absolute value)
        self.center_depth = max(self.para.center_depth, self.orth.center_depth)
        self.orth.color_depth = 'green'
        self.orth.color_z0 = 'green'
        self.subprofiles = [self.para, self.orth]

    @property
    def min_rim(self) -> float:
        """Minimum vertical location of the four parent crater rims.

        :rtype: float
        """
        return min(self.para.min_rim, self.orth.min_rim)

    @property
    def max_rim(self) -> float:
        """Maximum vertical location of the four parent crater rims.

        :rtype: float
        """
        return max(self.para.max_rim, self.orth.max_rim)

    @property
    def radius(self) -> float:
        """Averaged value of the two parent crater radii.

        :rtype: float
        """
        return .5 * (self.para.radius + self.orth.radius)

    @property
    def sigma_radius(self) -> float:
        """
        :return: Standard deviation of the radius.
        :rtype: float
        """
        if self._sigma_radius is None:
            self._sigma_radius = np.sqrt(0.25 * (self.para.sigma_radius ** 2 +
                                                 self.orth.sigma_radius ** 2))
        return self._sigma_radius

    @property
    def horizon(self) -> float:
        """Pad level at r -> infinity.

        :rtype: float
        """
        return .5 * (self.para.horizon + self.orth.horizon)

    def stdplot(self, ax, kwds=Profile.plotopts) -> None:
        """Creates a plot of the current crater state.
    
        :rtype : None
        :param kwds: options.
        :param ax: The axes object to plot into.
        :type ax: :class:`matplotlib.axes.AxesSubplot`
        """
        opts = kwds.copy()
        opts['setlabels'] = False
        for sp in self.subprofiles:
            sp.stdplot(ax, opts)
        try:
            if kwds['legend']:
                ax.legend(loc="lower left", frameon=False, fontsize='medium')
        except KeyError:
            pass
        kwds['xlabel'] = "x, y"
        self.set_plotlabels(ax, kwds)

    def _calcdV(self, xmax, n, z0, alpha):
        x = np.linspace(0, xmax, n + 1, True)
        x1, x2 = x[:-1], x[1:]
        z1 = -(self.para.depth(x1) - z0)
        z2 = -(self.orth.depth(alpha * x1) - z0)
        return 0.125 * pi * alpha * (x2 ** 2 - x1 ** 2) * (z1 + z2)

    def set_offset(self, offset):
        s = "This is an '%s' profile. Change offset in (one of) " % self.typ
        s += "the circular parent profiles."
        raise AttributeError(s)

    def dV(self, n: int=1000000, z='min') -> np.ndarray:
        """Elliptic volume elements. The `z` switch is used in the
        `Crater.minvol` and `Crater.maxvol` property methods.

        :param n: number of elements to create/calculate.
        :type n: int
        :param z: One of 'min', 'max', or a numeric z-value.
        :returns: Length-n array with the volume elements.
        :rtype : ndarray
        """
        ret = np.zeros(n)
        if z == 'min':
            mthz = min
        elif z == 'max':
            mthz = max
        else:
            mthz = lambda x, y: z
        # quadrant i
        alpha = self.orth.rim_2[0] / self.para.rim_2[0]
        z0 = mthz(self.para.rim_2[1], self.orth.rim_2[1])
        xmax = self.para.get_x(z0, half=2)
        ret += self._calcdV(xmax, n, z0, alpha)
        # quadrant ii
        alpha = self.orth.rim_2[0] / -self.para.rim_1[0]
        z0 = mthz(self.para.rim_1[1], self.orth.rim_2[1])
        xmax = -self.para.get_x(z0, half=1)
        ret += self._calcdV(xmax, n, z0, alpha)
        # quadrant iii
        alpha = -self.orth.rim_1[0] / -self.para.rim_1[0]
        z0 = mthz(self.para.rim_1[1], self.orth.rim_1[1])
        xmax = -self.para.get_x(z0, half=1)
        ret += self._calcdV(xmax, n, z0, alpha)
        # quadrant iv
        alpha = -self.orth.rim_1[0] / self.para.rim_2[0]
        z0 = mthz(self.para.rim_2[1], self.orth.rim_1[1])
        xmax = self.para.get_x(z0, half=2)
        ret += self._calcdV(xmax, n, z0, alpha)
        return ret


class NullCrater(object):
    """...
    """
    depth, minvol, maxvol, volume, sigma_vol, radius = 0., 0., 0., 0., 0., 0.
    summary = "depth:                       0.0000\n" + \
              "max rim height difference:   0.0000\n" + \
              "min volume:                  0.0000\n" + \
              "max volume:                  0.0000\n" + \
              "volume:                      0.0000 +- 0.000\n" + \
              "rel. deviation:              0.00 %\n"
    profile = NullProfile(typ="None")

    def __init__(self, **kwds):
        object.__init__(self)
        try:
            self.profile = kwds['profile']
        except KeyError:
            self.profile = EllipticNullProfile()

    def __str__(self):
        return self.summary


class EllipticNullProfile(NullProfile):
    """...
    """
    def __init__(self, **kwds):
        NullProfile.__init__(self, typ='elliptic')
        try:
            self.para = kwds['para']  # parallel profile
            self.orth = kwds['orth']  # orthogonal profile
        except KeyError:
            self.para = CircularNullProfile(typ='para')
            self.orth = CircularNullProfile(typ='orth')
        self.subprofiles = [self.para, self.orth]


class Crater(object):
    """A crater.
    Either profile or src must be given. If `src` is given, a `CircularProfile`
    is created.

    :keyword profile: A :class:`CircularProfile` or an \
     :class:`EllipticProfile` instance from which to 'create' the crater.
    :type profile: subclass of :class:`crater.Profile`
    :keyword src: A file name to load profile data from.
    """
    def __init__(self, **kwds):
        """See the class doc."""
        object.__init__(self)
        try:
            self.profile = kwds['profile']
        except KeyError:
            self.profile = CircularProfile(kwds['src'])
        self._maxvol, self._minvol, self._vol = None, None, None
        self._ref_vol = None        # Reference volume
        self._sigma_volume = None   # std-dev of volume
        self._sigmma_radius = None  # std-dev of radius

    def summary(self) -> str:
        """A summary.

        :returns: The summary as string.
        :rtype: str
        """
        s = "depth:                       %.4f\n" % self.depth
        s += "max rim height difference:   %.4f\n" \
            % (self.profile.max_rim - self.profile.min_rim)
        s += "min volume:                  %.4f\n" % self.minvol
        s += "max volume:                  %.4f\n" % self.maxvol
        s += "volume:                      %.4f +- %.4f\n" \
            % (self.volume, self.sigma_volume)
        s += "rel. deviation:              %.2f %s\n" \
            % (100 * self.sigma_volume / self.volume, "%")
        return s

    def show(self, fignum=1, opts=Profile.plotopts) -> None:
        """Prints the summary and plots the `stdplot`."""
        fig = plt.figure(fignum)
        print(self)
        self.profile.stdplot(fig.gca(), kwds=opts)
        fig

    @property
    def depth(self) -> float:
        """Depth of the crater

        :rtype: float
        """
        return self.profile.min_rim - self.profile.center_depth

    @property
    def minvol(self) -> float:
        """Minimum crater volume.

        :rtype: float
        """
        if self._minvol is None:
            self._minvol = np.sum(self.profile.dV(z='min'), axis=0)
        return self._minvol

    @property
    def maxvol(self) -> float:
        """Maximum crater volume.

        :rtype: float
        """
        if self._maxvol is None:
            self._maxvol = np.sum(self.profile.dV(z='max'), axis=0)
        return self._maxvol

    @property
    def volume(self) -> float:
        """Averaged crater volume.

        :rtype: float
        """
        if self._vol is None:
            self._vol = 0.5 * (self.minvol + self.maxvol)
        return self._vol

    @volume.deleter
    def volume(self):
        self._vol = None

    @property
    def sigma_volume(self) -> float:
        """Estimate of crater volumes standard deviation.

        :return: The standard deviation.
        """
        if self._sigma_volume is None:
            if isinstance(self.profile, EllipticProfile):
            # if self.profile.typ == 'elliptic':
                p = self.profile.para
                dt = (p.hrz, p.vrt)
                sdh, sdv = sd_length(dt[0]), sd_length(dt[1])
                data = (dt[0] + np.sign(dt[0]) * sdh, dt[1] - sdv)
                ppmin = CircularProfile(typ='parallel', data=data,
                                        extr=p.extr, centerindex=p.idx_center1)
                data = (dt[0] - np.sign(dt[0]) * sdh, dt[1] + sdv)
                ppmax = CircularProfile(typ='parallel', data=data, extr=p.extr,
                                        centerindex=p.idx_center1)
                p = self.profile.orth
                dt = (p.hrz, p.vrt)
                sdh, sdv = sd_length(dt[0]), sd_length(dt[1])
                data = (dt[0] + np.sign(dt[0]) * sdh, dt[1] - sdv)
                pomin = CircularProfile(typ='parallel', data=data, extr=p.extr,
                                        centerindex=p.idx_center1)
                data = (dt[0] - np.sign(dt[0]) * sdh, dt[1] + sdv)
                pomax = CircularProfile(typ='parallel', data=data, extr=p.extr,
                                        centerindex=p.idx_center1)
                pemin = EllipticProfile(para=ppmin, orth=pomin)
                pemax = EllipticProfile(para=ppmax, orth=pomax)
                cmin, cmax = Crater(profile=pemin), Crater(profile=pemax)
                self._sigma_volume = \
                    .68 ** 3 * np.abs(cmax.volume - cmin.volume)
            else:
                dt = (self.profile.hrz, self.profile.vrt)
                sdh, sdv = sd_length(dt[0]), sd_length(dt[1])
                data = (dt[0] + np.sign(dt[0]) * sdh, dt[1] - sdv)
                pmin = CircularProfile(typ=self.profile.typ, data=data)
                data = (dt[0] - np.sign(dt[0]) * sdh, dt[1] + sdv)
                pmax = CircularProfile(typ=self.profile.typ, data=data)
                cmin, cmax = Crater(profile=pmin), Crater(profile=pmax)
                self._sigma_volume = \
                    .68 ** 3 * np.abs(cmax.volume - cmin.volume)
        return self._sigma_volume

    @sigma_volume.deleter
    def sigma_volume(self):
        self._sigma_volume = None

    @property
    def ref_vol(self) -> float:
        """Crater volume with respect to pad surface.

        :rtype: float
        """
        if self._ref_vol is None:
                self._ref_vol = np.sum(self.profile.dV(z=self.profile.horizon),
                                       axis=0)
        else:
            self._ref_vol = 0.
        return self._ref_vol

    @ref_vol.deleter
    def ref_vol(self):
        self._ref_vol = None

    @property
    def radius(self) -> float:
        """Crater radius. 'Forwarded' from `Crater.profile.radius`.

        :rtype: float
        """
        return self.profile.radius

    @property
    def sigma_radius(self) -> float:
        """
        :return: standard deviation of crater radius. 'Forwarded' from
         :class:`Crater.profile`.
        :rtype: float
        """
        return self.profile.sigma_radius

    def __str__(self):
        return self.summary()


nullprofile = NullProfile(typ="none")
