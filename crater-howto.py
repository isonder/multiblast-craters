
# coding: utf-8

# **Legal blurb:**  
# The modules `crater.py` and `blasts.py` are published under the Gnu General public licence version 2
# 
# # Remarks
# 
# The code tries to roughly separate the geometric functionality from the capabilities to handle blast history, and energy related problems. To compute blast properties, the following information is necessary:
# 
# - A crossectional elevation profile of a crater
# - The blast energy
# - The location of the blast energy
# - optional: some information about the pre-blast topography (as profile data)
# 
# With this information 3 different objects are created for each blast:
# 
# - A `Profile` object  
#   Provides generic geometry information such as e.g. depth, as well as some functionality for volume calculation.
#   There are several profile types: A `CirularProfile` (Assumes circular symmetry for volume and radius calculations), an
#   `EllipticProfile` which is a compound profile made from two `CircularProfile`s, and assumes that those have a 90 degrees
#   angle (assumes elliptic symmetry). There is also a `DEM` in development (not included here) that will represent a 3d
#   model of a crater.
# - A `Crater` object  
#   Provides specific crater geometry, such as radius, volume, depth. A `Crater` object needs a `Profile` object as input
#   (see below).
# - A `Blast` object  
#   Here the geometry information is merged with energy and blast history. The `Blast` object does not necessarily need a
#   `Crater` as input. In such a case, a crater radius and volume can be specified manually. A `Blast` can have another
#   `Blast` as 'parent', in which case some cumulative properties (e.g. energy) are available for analysis, which is
#   necessary to clearly distnguish between one-blast and multi-blast structures.
# 
# More features can be found in the script code in the files `crater.py` and `blasts.py`.
# 
# *Note that the code does not include any unit conversions. It should, in principle work fine with imperial or any other unit system, but this is untested. The examples below use SI units*.

#   
#   
# This line loads the plotting backend. If is does not work try to comment out line 1 and uncomment line 2.

# In[1]:
import matplotlib.pyplot as plt
plt.ioff()
#get_ipython().magic('matplotlib notebook')
##%matplotlib inline
#from IPython.display import set_matplotlib_formats
#set_matplotlib_formats('svg')


# **import the `crater` module and `Blast` object** and some other stuff

# In[2]:

import crater
from blasts import (Blast,    # the Blast object
                    ot,       # one third
                    kgplb,    # unit conversion
                    spec_en)  # specific energy of PETN (J/kg)

print(" ot      = %s\n" % ot,
      "kgplb   = %s\n"  % kgplb,
      "spec_en = %s"    % spec_en)


# ## load profile data from text file
# 
# Import the numpy library (only used here to read the text files).
import numpy as np


# Read the text files into arrays. Data is given as $(x,y)$-pairs, where $x$ is a horizontal and $y$ a vertical coordinate. Have a look at the the text files in the `data` directory. Location units are meters.
data41 = np.loadtxt('data/2014-crater-profiles_p4b1.txt',
                    skiprows=1)
data42 = np.loadtxt('data/2014-crater-profiles_p4b2.txt',
                    skiprows=1)


# # `Crater` object
# 
# ## create profile objects
p41 = crater.CircularProfile(data=(data41[:, 0], data41[:, 1]))
p42 = crater.CircularProfile(data=(data42[:, 0], data42[:, 1]))


# ## create crater objects from profiles
c41 = crater.Crater(profile=p41)
c42 = crater.Crater(profile=p42)


#Show some crater information. The `fignum` argument is only necessary
#to distinguish between different plots.
c41.show(fignum=410)
plt.show()


# # `Blast` object
b41 = Blast(4, 1, parent=None, energy=ot * spec_en * kgplb,
            dob=.46, desc="session 2014", crater=c41)
b42 = Blast(4, 2, parent=b41, energy=ot * spec_en * kgplb,
            dob=.5, desc="session 2014", crater=c42)
b42.show(fignum=421)
plt.show(421)


print("\n### control volume")
print("b41.control_vol: ", b41.control_vol)
print("b42.control_vol: ", b42.control_vol)


print("\n### radius")
print("Crater radius, and change of crater radius")
print("b41.radius: ", b41.radius, "b42.radius: ", b42.radius)
print("b42.radius_change: ", b42.radius_change)


print("\n### energy")
print("Energy of a particular blast, and cumulative energy of a blast and its preceding blasts (parents).")
print("b41.energy: ", b41.energy, "b42.energy: ", b42.energy)
print("b42.energy_cumul: ", b42.energy_cumul)


print("\n### volume")
print("total crater volume, and volume change caused by the blast")
print("b41.volume: ", b41.volume, "b42.volume: ", b42.volume)
print("b42.created_volume: ", b42.created_volume)


print("\n### confinement potential $U$")
print("Confinement potential $U$, and confinement potential in flat topography U_0.")
print("b41.u: ", b41.u, "b41.u0: ", b41.u0)
print("b42.u: ", b42.u, "b42.u0: ", b42.u0)


print("\n### average confinement force F")
print("Average confinement force $F$, and average confinement force in flat topography $F_0$.")
print("b41.f: ", b41.f, "b41.f0: ", b41.f0)
print("b42.f: ", b42.f, "b42.f0: ", b42.f0)


print("\n### (effective) explosion depth")
print("Distance of charge location to pre-blast crater bottom d, and effective explosion depth below crater bottom d_eff.")
print("b41.dob: ", b41.dob, "b41.deff: ", b41.deff)
print("b42.dob: ", b42.dob, "b42.deff: ", b42.deff)

