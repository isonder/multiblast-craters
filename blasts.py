# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 16:50:42 2013

:synopsis: Blast specific funcionality. Prepared for 'blast inheritance'.

:moduleauthor: ingo

**Random notes**:
Currently the integration to calculate *u* and *f* is carried out over the pad
dimensions. Outside the pad, the profiles are assumed to have the *horizon*
elevation (equivalent to flat topography). The typically very small residual
value from this contribution is calculated analytically, and added in the case
it's absolute value is larger that 1e-5 times the absolute value of *u* or *f*
as calculated from the pad only.

TODO: The Blast class is too big and should be separated into some container
with methods for computation (see the commented 'Compute' class below), and the
Blast object, that will mostly hold the data and manage some small logical
interaction of parameters.
"""


# from builtins import print
import numpy as np
from numpy import array, pi, abs, sqrt
import crater as crat

debug = 0  # Debug level. 0 for no debugging.

ot = 1. / 3
tt = 2. / 3
kgplb = 0.45359237  # kg per lb
spec_en = 4.85E+6   # specific energy of Pentex / J per kg


def get_vol(blasts):
    return array([blast.volume for blast in blasts])


def get_energy(blasts):
    return array([blast.energy for blast in blasts])


def get_energy_cumul(blasts):
    return array([blast.energy_cumul for blast in blasts])


def get_radii(blasts):
    return array([blast.energy_cumul for blast in blasts])


def get_radii_changes(blasts):
    return array([blast.radius_change for blast in blasts])


def rbar(dbar, dbopt, rb00, rbmax, b=1.):
    """
    :param dbar: Scaled depth.
    :param dbopt: Optimum scaled depth.
    :param rb00: Zero-depth scaled radius.
    :param rbmax: Maximum scaled radius.
    :param b: Decay constant.
    :return: Scaled radius.
    """
    # dbar is float
    if not isinstance(dbar, np.ndarray):
        if dbar <= dbopt:
            ret = -(rbmax - rb00) \
                * (dbar / dbopt - 1.) ** 2 + rbmax
        else:
            ret = rbmax / \
                sqrt(1. + ((rbmax / b)
                              * (dbar - dbopt)) ** 2)
    # dbar is array
    else:
        ret = np.empty_like(dbar)
        idx = dbar <= dbopt
        ret[idx] = -(rbmax - rb00) \
            * (dbar[idx] / dbopt - 1.) ** 2 + rbmax
        idx = np.negative(idx)
        ret[idx] = rbmax / sqrt(
            1. + ((rbmax / b) * (dbar[idx] - dbopt)) ** 2)
    return ret


def sigma_rbar(dbar: tuple, dbopt: tuple, rb00: tuple,
               rbmax: tuple, b: tuple=(1., 0)):
    """
    :param dbar: Scaled depth and its standard deviation.
    :param dbopt: Optimum scaled depth and its standard deviation.
    :param rb00: Zero-depth scaled radius and its standard deviation.
    :param rbmax: Optimum/maximum scaled radius and its standard deviation.
    :param b: Decay constant and its standard deviation.
    :return: Standard deviation of rbar(dbar).
    """
    dbar, sdbar = dbar
    dbopt, sdbopt = dbopt
    rb00, srb00 = rb00
    rbmax, srbmax = rbmax
    b, sb = b
    if not isinstance(dbar, np.ndarray):
        if dbar < dbopt:
            x = dbar / dbopt - 1
            fact = (rbmax - rb00) * 2 * x / dbopt
            ret = (srbmax * (x ** 2 + 1)) ** 2 + (srb00 * x ** 2) ** 2 + \
                  (sdbopt * fact / dbopt) ** 2 + (sdbar * fact) ** 2
        else:
            rb0 = rbar(dbar, dbopt, rb00, rbmax, b)
            ret = (srbmax * (rb0 / rbmax *
                             (1 - rb0 * rb0
                              * ((dbar - dbopt) / b) ** 2))) ** 2 + \
                  (sdbar * rb0 ** 3 * dbar / (rbmax * b)) ** 2 + \
                  (sdbopt * rb0 ** 3 * dbopt / (rbmax * b)) ** 2 + \
                  (sb * 2 * rb0 ** 3 * (dbar - dbopt) ** 2 / b ** 3) ** 2
    else:
        ret = np.empty_like(dbar)
        idx = dbar <= dbopt
        x = dbar[idx] / dbopt - 1
        fact = (rbmax - rb00) * 2 * x / dbopt
        ret[idx] = (srbmax * (x ** 2 + 1)) ** 2 + (srb00 * x ** 2) ** 2 + \
                   (sdbopt * fact / dbopt) ** 2 + (sdbar * fact) ** 2
        idx = np.negative(idx)
        rb0 = rbar(dbar[idx], dbopt, rb00, rbmax, b)
        ret[idx] = (srbmax * (rb0 / rbmax *
                              (1 - rb0 * rb0
                               * ((dbar[idx] - dbopt) / b) ** 2))) ** 2 + \
                   (sdbar * rb0 ** 3 * dbar[idx] / (rbmax * b)) ** 2 + \
                   (sdbopt * rb0 ** 3 * dbopt / (rbmax * b)) ** 2 + \
                   (sb * 2 * rb0 ** 3 * (dbar[idx] - dbopt) ** 2 / b ** 3) ** 2
    return sqrt(ret)


class FlatbedRadius(object):
    """
    Model of the flat bed scaled crater radius.
    """

    def __init__(self,
                 rb00: float=1., sd_rb00: float=0.,
                 rbmax: float=1., sd_rbmax: float=0.,
                 dbopt: float=1., sd_dbopt: float=0.,
                 b: float=1., sd_b: float=0.):
        """
        :param rb00: zero depth scaled radius
        :type rb00: float
        :param rbmax: scaled radius at optimum depth
        :type rbmax: float
        :param dbopt: optimum scaled depth
        :type dbopt: float
        :param b: decay constant for large depth
        :type b: float
        """
        object.__init__(self)
        self.rb00 = rb00
        self.sd_rb00 = sd_rb00
        self.rbmax = rbmax
        self.sd_rbmax = sd_rbmax
        self.dbopt = dbopt
        self.sd_dbopt = sd_dbopt
        self.b = b
        self.sd_b = sd_b

    def model(self, dbar):
        return rbar(dbar, self.dbopt, self.rb00, self.rbmax, self.b)

    def standard_deviation(self, dbar, sd_dbar):
        return sigma_rbar((dbar, sd_dbar), (self.dbopt, self.sd_dbopt),
                          (self.rb00, self.sd_rb00),
                          (self.rbmax, self.sd_rbmax), (self.b, self.sd_b))

    def __call__(self, dbar):
        return self.model(dbar)

    def __str__(self):
        s = "Flat bed scaled radius.\nParameters:"
        s += "\n  rb00 (0-depth scaled radius): %.3e mJ^-1/3" % self.rb00
        s += "\n  rbmax (max. scaled radius):   %.3e mJ^-1/3" % self.rbmax
        s += "\n  dbopt (optimum scaled depth): %.3e mJ^-1.3" % self.dbopt
        s += "\n  b (decay):                    %.3e mJ^-1/3" % self.b
        return s


default_fbr = FlatbedRadius(rb00=2.38201627e-03, sd_rb00=8.67012310e-04,
                            rbmax=7.51245150e-03, sd_rbmax=3.31369839e-04,
                            dbopt=3.85e-03, sd_dbopt=0.,
                            b=1.72140675e-05, sd_b=2.05913851e-06)


class Blast(object):
    """
    A container connecting current morphology and energy with previous ones
    at same location.

    :param pad: The pad number
    :type pad: int
    :param run: The blast number
    :type run: int
    :param parent: Preceding blast object
    :type parent: :class:`Blast`
    :param energy: Charge energy
    :type energy: float
    :param dob: Depth of burial below crater surface at center
    :type dob: float
    :param crater: Corresponding crater object, if it exists
    :type crater: :class:`crater.Crater`
    :param desc: Minimal description fo identification.
    :type desc: :class:`str`

    .. attribute:: pad

        Pad number. (int)

    .. attribute:: run

        Blast/run number. (int)

    .. attribute:: energy_cumul

        Cumulative energy: Sum of this and all preceding blasts of this
        location. (float)

    .. attribute:: plotcolor

        A (html) color value for plotting. (str)

    .. attribute:: plotchar

        A marker symbol for plotting. (str)

    .. attribute:: dob

        Depth of charge below crater center. (int)

    .. attribute:: n

        Blast number. (int)

    """

    dbopt = 3.85e-3         # Optimum scaled depth of burial
    E0 = (.5 / dbopt) ** 3  # Base for energy scaling
    dboc = dbopt ** 3
    # asq = 2.6734909633341512e-05  # Fit constant of crater radius model.

    def __init__(self, pad=1, run=1, parent=None,
                 energy=1.0, dob=0., crater=None, parent_crater=None,
                 desc=""):
        """
        See above.
        """
        object.__init__(self)
        self._sigma_radius = None
        self.pad = pad
        self.run = run
        self.desc = desc
        self.parent = parent
        self.energy = energy
        self.energy_cumul = energy
        self.plotcolor = 'red'
        self.plotchar = 'o'
        if self.parent is not None:
            self.energy_cumul += self.parent.energy_cumul
            self.plotcolor = 'blue'
        self.dob = dob
        self.sigma_dob = crat.sd_length(dob, mode='subsurf')
        self.crater = crater
        self.n = 1  # blast number of a series
        self.parent_crater = parent_crater
        if self.parent is not None:
            self.parent_crater = self.parent.crater
            self.n += self.parent.n
        self.calcprof = None
        if self.parent_crater is not None:
            try:
                self.calcprof = self.parent_crater.profile.para
            except AttributeError:
                self.calcprof = self.parent_crater.profile
        self.nint = 1000000
        # Private storage for dynamically calculated properties
        self._volume = None
        self._rpad = None
        self._z0 = None
        self._sigma_z0 = None
        self._control_vol = None
        self._sigma_control_vol = None
        self._vp = None
        self._deff = None
        self._sigma_deff = None
        self._f0 = None
        self._u0 = None
        self._sigma_u0 = None
        self._f = None
        self._sigma_f0 = None
        self._sigma_f = None
        self._u = None
        self._sigma_u = None
        self._radius_change = None
        self._sigma_radius_change = None
        self._radius = None
        self._r0 = None
        self._sigma_r0 = None
        self._rbmodel = None
        self._dbar_avg = None
        self._control_cm = None
        self._cm_change = None
        self._reference_vol = None
        self._ref_vol_change = None
        self._created_vol = None
        self._sigma_created_vol = None
        # The parameters
        self._alpha = 1.  # Radius parameter
        self._gamma = 1.  # Potential exponent
        self._eps = 1.  # Energy parameter

    def show(self, fignum=1):
        print(self)
        s = "effective depth: %s\n" % self.deff
        s += "volume, created volume: %s, %s\n" \
             % (self.volume, self.created_volume)
        s += "radius, radius change: %s, %s\n" \
             % (self.radius, self.radius_change)
        print(s)
        if self.crater is not None:
            self.crater.show(fignum)

    def integrate_uncert(self, dfdx, dfdy, n=1000000) -> float:
        """Generic uncertainty integrator.
        :param f: The integrand.
        :type f: function(x).
        :param dfdx: Derivative of integrand with respect to x.
        :param dfdy: Derivative of integrand with respect to y.
        :return: The uncertainty.
        """
        x = np.linspace(0, self.rpad, n)
        if self.calcprof is not None:
            dzsdx = self.calcprof.depth.derivative()(x)
            sdzssq = crat.sd_length(self.calcprof.depth(x)) ** 2
        else:
            dzsdx, sdzssq = 0., 0.
        sdxsq = crat.sd_length(x) ** 2
        sdysq = (sdzssq + self.sigma_zc(x) ** 2) * \
            np.exp(-2 * np.abs(x) / self.r0)
        dfdyval = dfdy(x)
        ret = np.trapz(
            sdxsq * (dfdx(x) - dfdyval * (self.dzcdr(x) - dzsdx)) ** 2 +
            sdysq * dfdyval ** 2, x=x)
        return 0.5 * np.sqrt(ret)

    def zc(self, r) -> np.ndarray:
        """
        :returns: Elevation of control volume.
        :rtype : np.ndarray
        :param r: Radial distance to center
        """
        try:
            h = self.calcprof.horizon
        except AttributeError:
            h = self.crater.profile.horizon
        return self.z0 \
            * np.exp(-self.dob * r ** 2 /
                     (self.dbopt * (self.eps * self.energy) ** ot *
                      self.r0 ** 2)) + h

    def dzcdr(self, r) -> np.ndarray:
        """..."""
        dopt = self.dbopt * (self.eps * self.energy) ** ot
        return -2 / self.r0 * (self.dob * r / (dopt * self.r0)) * \
            self.z0 * np.exp(-self.dob * r ** 2 / (dopt * self.r0 ** 2))

    def sigma_zc(self, r: np.ndarray) -> np.ndarray:
        """
        Standard deviation of control shape elevation.
        :param r: Radius coordinate
        :return: Standard deviation of control shape.
        """
        dopt = self.dbopt * (self.eps * self.energy) ** ot
        ret = self.zc(r) ** 2 * (
            (self.sigma_z0 / self.z0) ** 2 +
            (-r ** 2 * self.sigma_dob / (self.r0 ** 2 * dopt)) ** 2 +
            (-2 * self.dob * r ** 2 * self.sigma_r0 /
             (dopt * self.r0 ** 3)) ** 2
        )
        return sqrt(ret)

    @property
    def alpha(self) -> float:
        """
        :returns: Radius scaling parameter
        :rtype: float
        """
        return self._alpha

    @alpha.setter
    def alpha(self, val):
        if self._alpha != val:
            self._alpha = val
            del self.r0

    @alpha.deleter
    def alpha(self):
        self._alpha = None
        del self.r0

    @property
    def gamma(self) -> float:
        """
        Confinement potential exponent.

        :rtype: float
        """
        return self._gamma

    @gamma.setter
    def gamma(self, value):
        if self._gamma != value:
            self._gamma = value
            del self.z0

    @gamma.deleter
    def gamma(self):
        self._gamma = None
        del self.z0

    @property
    def eps(self) -> float:
        """Energy/depth scaling parameter.

        :rtype: float
        """
        return self._eps

    @eps.setter
    def eps(self, val):
        if self._eps != val:
            self._eps = val
            del self.f0, self.u0, self.control_vol

    @eps.deleter
    def eps(self):
        self._eps = None
        del self.f0, self.u0, self.control_vol

    @property
    def dob_cumul(self):
        ret = self.dob
        if self.parent is not None:
            ret += self.parent.dob_cumul
        return ret

    @property
    def deff(self) -> float:
        """Effective explosion depth.

        :rtype: float
        """
        if self._deff is None:
            if self.parent_crater is None:
                self._deff = self.dob
            else:
                self._deff = self.u / self.f
        return self._deff

    @deff.deleter
    def deff(self):
        self._deff = None

    @property
    def sigma_deff(self) -> float:
        if self._sigma_deff is None:
            ret = self.deff * sqrt(
                (self.sigma_u / self.u) ** 2 + (self.sigma_f / self.f) ** 2)
            if self.parent_crater is not None:
                self._sigma_deff = ret
            else:
                self._sigma_deff = min(ret, self.sigma_dob)
        return self._sigma_deff

    @sigma_deff.deleter
    def sigma_deff(self):
        self._sigma_deff = None

    @property
    def deff_cumul(self) -> float:
        """Cumulative effective depth.

        :rtype: float
        """
        ret = self.deff
        if self.parent is not None:
            ret += self.parent.deff_cumul
        return ret

    @property
    def dbar_avg(self) -> float:
        """Average efective depth of burial. Derived from cumulative values
        of u and f.

        :rtype: float
        """
        if self._dbar_avg is None:
            self._dbar_avg = self.u_cumul \
                / (self.f_cumul * (self.energy_cumul / self.n) ** ot)
        return self._dbar_avg

    @property
    def volume(self) -> float:
        """
        :returns: Volume of the blasts crater structure.
        :rtype : float
        """
        if self._volume is None:
            if self.crater is not None:
                self._volume = self.crater.volume
        return self._volume

    @volume.deleter
    def volume(self):
        self._volume = None

    @property
    def sigma_volume(self) -> float:
        ret = None
        if self.crater is not None:
            ret = self.crater.sigma_volume
        return ret

    @property
    def created_volume(self) -> float:
        """
        :return: Volume difference between the blasts parent (if any) and
         this blasts.
        :rtype : float
        """
        if self._created_vol is None:
            if self.parent is None:
                self._created_vol = self.volume
            else:
                self._created_vol = self.volume - self.parent.volume
        return self._created_vol

    @created_volume.deleter
    def created_volume(self):
        self._created_vol = None

    @property
    def sigma_created_volume(self) -> float:
        if self._sigma_created_vol is None:
            if self.parent is not None:
                self._sigma_created_vol = \
                    sqrt(self.sigma_volume ** 2 +
                         self.parent.sigma_volume ** 2)
            else:
                self._sigma_created_vol = self.sigma_volume
        return self._sigma_created_vol

    @sigma_created_volume.deleter
    def sigma_created_volume(self):
        self._sigma_created_vol = None

    @property
    def z0(self) -> float:
        """Amplitude of weight function / control volume.

        :rtype: float
        """
        if self._z0 is None:
            if self.parent_crater is not None:
                zp0 = self.calcprof.center_depth - self.calcprof.horizon
            else:
                zp0 = 0.
            # Charge location in potential coordinate system
            # zp0 += -self.dob
            self._z0 = -((self.gamma + 1.) ** 2 / self.gamma) * self.dob + zp0
        return self._z0

    @z0.deleter
    def z0(self):
        self._z0 = None
        del self.control_vol, self.u0, self.f0

    @property
    def sigma_z0(self) -> float:
        if self._sigma_z0 is None:
            if self.parent_crater is not None:
                sdzhsq = self.calcprof.sigma_horizon ** 2 + \
                    crat.sd_length(self.calcprof.center_depth) ** 2
            else:
                sdzhsq = 0.
            self._sigma_z0 = sqrt(((self.gamma + 1) ** 2 / self.gamma *
                                   self.sigma_dob) ** 2 + sdzhsq)
        return self._sigma_z0

    @sigma_z0.deleter
    def sigma_z0(self):
        self._sigma_z0 = None

    @property
    def f0(self) -> float:
        """Confinement force at zero topography.
        :rtype: float
        """
        if self._f0 is None:
            self._f0 = -pi * np.sign(self.z0) * abs(self.z0) ** self.gamma \
                * self.dbopt * (self.eps * self.energy) ** ot * self.r0 ** 2 \
                / (self.gamma * self.dob)
        return self._f0

    @f0.deleter
    def f0(self):
        self._f0 = None
        del self.f

    @property
    def sigma_f0(self) -> float:
        if self._sigma_f0 is None:
            try:
                def f(x):
                    zs = self.crater.profile.horizon
                    valu = -(self.zc(x) - zs)
                    valu[valu < 0.] = 0.
                    return -2 * pi * x * valu ** self.gamma

                def dfdr(x):
                    valu = -(self.zc(x) - self.crater.profile.horizon)
                    valu[valu < 0.] = 0.
                    return -2 * pi * valu ** (self.gamma - 1) * (
                        valu + self.gamma * x * self.dzcdr(x)
                    )

                def dfdy(x):
                    y = -(self.zc(x) - self.crater.profile.horizon)
                    y[y <= 0.] = 0.
                    return -2 * pi * self.gamma * x * y ** (self.gamma - 1)

                self._sigma_f0 = self.integrate_uncert(dfdr, dfdy)
            except AttributeError:
                self._sigma_f0 = self.f0 * sqrt(
                    (self.sigma_dob / self.dob) ** 2 +
                    (self.gamma * self.sigma_z0 / self.z0) ** 2 +
                    (2 * self.sigma_r0 / self.r0) ** 2
                )
        return self._sigma_f0

    @sigma_f0.deleter
    def sigma_f0(self):
        self._sigma_f0 = None

    @property
    def f(self) -> float:
        """Confinement force at non-zero topography.

        :rtype: float
        """
        if self._f is None:
            if self.parent_crater is not None:
                x = np.linspace(-self.rpad, self.rpad, self.nint)
                intgr = -(self.zc(x) - self.calcprof.depth(x))
                # print(np.average(intgr), min(intgr), max(intgr))
                intgr[intgr <= 0.] = 0.
                self._f = pi * np.trapz(abs(x) * intgr ** self.gamma, x)
                fr = self.f0 * np.exp(
                    -self.gamma * self.dob * self.rpad ** 2 /
                    ((self.eps * self.energy) ** ot * self.dbopt * self.r0 ** 2)
                )
                if abs(fr) > 1e-5 * abs(self._f):
                    self._f += fr
                if debug:
                    print("f = %s, Fr = %s" % (self._f, fr))
            else:
                self._f = self.f0
        return self._f

    @f.deleter
    def f(self) -> None:
        self._f = None
        del self.deff

    @property
    def sigma_f(self) -> float:
        if self._sigma_f is None:
            if self.parent_crater is not None:
                def f(x):
                    zs = self.calcprof.depth(x)
                    valu = -(self.zc(x) - zs)
                    valu[valu < 0.] = 0.
                    return -2 * pi * x * valu ** self.gamma

                def dfdr(x):
                    zs = self.calcprof.depth(x)
                    valu = -(self.zc(x) - zs)
                    valu[valu < 0.] = 0.
                    dzsdr = self.calcprof.depth.derivative()
                    return -2 * pi * valu ** (self.gamma - 1) * (
                        valu + self.gamma * x * (dzsdr(x) - self.dzcdr(x))
                    )

                def dfdy(x):
                    y = -(self.zc(x) - self.calcprof.depth(x))
                    y[y <= 0.] = 0.
                    return -2 * pi * self.gamma * x * y ** (self.gamma - 1)

                self._sigma_f = self.integrate_uncert(dfdr, dfdy)
            else:
                self._sigma_f = self.sigma_f0
        return self._sigma_f

    @sigma_f.deleter
    def sigma_f(self):
        self._sigma_f = None
        del self.sigma_deff

    @property
    def f_cumul(self) -> float:
        """Cumulative confinement force.

        :type: float
        """
        ret = self.f
        if self.parent is not None:
            ret += self.parent.f_cumul
        return ret

    @property
    def u0(self) -> float:
        """Confinement potential at zero topography.

        :type: float
        """
        if self._u0 is None:
            gp1 = self.gamma + 1.
            self._u0 = -pi * self.dbopt * (self.eps * self.energy) ** ot \
                * np.sign(self.z0) * abs(self.z0) ** gp1 * self.r0 ** 2 \
                / (gp1 ** 2 * self.dob)
        return self._u0

    @u0.deleter
    def u0(self):
        self._u0 = None
        del self.u

    @property
    def sigma_u0(self) -> float:
        if self._sigma_u0 is None:
            try:
                def fu(x):
                    gp1 = self.gamma + 1.
                    y = -(self.zc(x) - self.crater.profile.horizon)
                    y[y <= 0.] = 0.
                    return -(2 * pi / gp1) * x * y ** gp1

                def dfudx(x):
                    y = -(self.zc(x) - self.crater.profile.horizon)
                    y[y <= 0.] = 0.
                    return -2 * pi * y ** self.gamma * (
                        y / (self.gamma + 1.) + x * self.dzcdr(x)
                    )

                def dfudy(x):
                    y = -(self.zc(x) - self.crater.profile.horizon)
                    y[y <= 0.] = 0.
                    return 2 * pi * x * y ** self.gamma

                self._sigma_u0 = self.integrate_uncert(dfudx, dfudy)
            except AttributeError:
                gp1 = self.gamma + 1
                self._sigma_u0 = sqrt(
                    (gp1 * self.u0 * self.sigma_z0 / self.z0) ** 2 +
                    (self.u0 * self.sigma_dob / self.dob) ** 2 +
                    (2 * self.u0 * self.sigma_r0 / self.r0) ** 2
                )
        return self._sigma_u0

    @sigma_u0.deleter
    def sigma_u0(self) -> None:
        self._sigma_u0 = None
        del self.sigma_deff

    @property
    def u(self) -> float:
        """Confinement potential at non-zero topography.

        :rtype: float
        """
        if self._u is None:
            if self.parent_crater is not None:
                x = np.linspace(-self.rpad, self.rpad, self.nint)
                gp1 = self.gamma + 1.
                intgr = -(self.zc(x) - self.calcprof.depth(x))
                intgr[intgr < 0.] = 0.
                self._u = (pi / gp1) * np.trapz(abs(x) * intgr ** gp1, x)
                ur = self.u0 * np.exp(
                    -gp1 * self.dob * self.rpad ** 2 /
                    ((self.eps * self.energy) ** ot * self.dbopt * self.r0 ** 2)
                )
                if abs(ur) > 1e-5 * abs(self._u):
                    self._u += ur
                if debug:
                    print("u = %s, ur = %s" % (self._u, ur))
            else:
                self._u = self.u0
        return self._u

    @u.deleter
    def u(self) -> None:
        self._u = None
        del self.deff

    @property
    def sigma_u(self) -> float:
        if self._sigma_u is None:
            if self.parent_crater is not None:
                def fu(x):
                    gp1 = self.gamma + 1.
                    y = -(self.zc(x) - self.calcprof.depth(x))
                    y[y <= 0.] = 0.
                    return -(2 * pi / gp1) * x * y ** gp1

                def dfudx(x):
                    y = -(self.zc(x) - self.calcprof.depth(x))
                    y[y <= 0.] = 0.
                    dzsdx = self.calcprof.depth.derivative()
                    return -2 * pi * y ** self.gamma * (
                        y / (self.gamma + 1.) +
                        x * (self.dzcdr(x) - dzsdx(x))
                    )

                def dfudy(x):
                    y = -(self.zc(x) - self.calcprof.depth(x))
                    y[y <= 0.] = 0.
                    return 2 * pi * x * y ** self.gamma

                self._sigma_u = self.integrate_uncert(dfudx, dfudy)
            else:
                self._sigma_u = self.sigma_u0
        return self._sigma_u

    @property
    def u_cumul(self) -> float:
        """Cumulative confinement potential.

        :type: float
        """
        ret = self.u
        if self.parent is not None:
            ret += self.parent.u_cumul
        return ret

    @property
    def sigma_u_cumul(self) -> float:
        ret = self.sigma_u
        if self.parent is not None:
            ret = sqrt(ret ** 2 + self.parent.sigma_u_cumul ** 2)
        return ret

    @property
    def rpad(self) -> float:
        """
        :returns: Pad size.
        :rtype: float
        """
        if self._rpad is None:
            p = self.calcprof
            if p is None:
                try:
                    p = self.crater.profile.para
                except AttributeError:
                    p = self.crater.profile
            self._rpad = .5 * (abs(p.hrz[0]) + p.hrz[-1])
        return self._rpad

    @rpad.setter
    def rpad(self, val: float):
        del self.u, self.f
        self._rpad = float(val)

    @rpad.deleter
    def rpad(self):
        self._rpad = None
        del self.u, self.f

    @property
    def control_vol(self) -> float:
        """Control volume

        :type: float
        """
        if self._control_vol is None:
            vc0 = -pi * self.z0 * self.r0 ** 2 * \
                (self.eps * self.energy) ** ot * self.dbopt / self.dob
            # print("Vc0 = %s" % Vc0)
            self._control_vol = vc0 - self.vp
        return self._control_vol

    @control_vol.deleter
    def control_vol(self):
        self._control_vol = None

    @property
    def sigma_control_vol(self) -> float:
        """Standard deviation of control volume"""
        if self._sigma_control_vol is None:
            if self.parent_crater is not None:
                def f(x):
                    p = self.calcprof
                    val = -2 * pi * abs(x) * (self.zc(x) - p.depth(x))
                    val[val <= 0.] = 0.
                    return val

                def dfdr(x):
                    p = self.calcprof
                    zs = p.depth(x)
                    dzsdr = p.depth.derivative()
                    val = self.zc(x) - zs
                    val[val <= 0.] = 0.
                    return 2 * pi * (val + abs(x) *
                                     (self.dzcdr(x) - dzsdr(x)))

                def dfdy(x):
                    return -2 * pi * abs(x)

                self._sigma_control_vol = self.integrate_uncert(f, dfdr, dfdy)
            else:
                self._sigma_control_vol = self.control_vol * sqrt(
                    (self.sigma_z0 / self.z0) ** 2 +
                    (2 * self.sigma_r0 / self.r0) ** 2 +
                    (self.sigma_dob / self.dob) ** 2
                )
        return self._sigma_control_vol

    @sigma_control_vol.deleter
    def sigma_control_vol(self):
        self._sigma_control_vol = None

    @property
    def vp(self):
        """
        """
        if self._vp is None:
            if self.parent_crater is not None:
                try:
                    p = self.parent_crater.profile.para
                except AttributeError:
                    p = self.parent_crater.profile
                self._vp = abs(np.sum(
                    pi * 0.25 * (p.hrz[1:] ** 2 - p.hrz[:-1] ** 2)
                    * (0.5 * (p.vrt[1:] + p.vrt[:-1]) - p.horizon), axis=0))
            else:
                self._vp = 0.
        return self._vp

    @vp.deleter
    def vp(self):
        self._vp = None
        del self.control_vol, self.control_cm

    @property
    def control_vol_cumul(self):
        """Cumulative control volume

        :type: float
        """
        ret = self.control_vol
        if self.parent is not None:
            ret += self.parent.control_vol_cumul
        return ret

    @property
    def control_cm(self):
        """Center of mass of the control volume.

        :type: float
        """
        if self._control_cm is not None:
            return self._control_cm
        else:
            tmp = self.gamma
            if tmp != 1.:
                self.gamma = 1.
            self._control_cm = self.u / self.control_vol
            self.gamma = tmp
            return self._control_cm

    @control_cm.deleter
    def control_cm(self):
        self._control_cm = None

    @property
    def cm_change(self):
        if self._cm_change is None:
            cm = 0.
            if self.parent is not None:
                cm = self.parent.control_cm
            self._cm_change = self.control_cm - cm
        return self._cm_change

    @cm_change.deleter
    def cm_change(self):
        self._cm_change = None

    @property
    def reference_vol(self):
        if self._reference_vol is None:
            if self.parent_crater is not None:
                try:
                    p = self.parent_crater.profile.para
                except AttributeError:
                    p = self.parent_crater.profile
                x = np.linspace(p.hrz[0], p.hrz[-1], 1000000)
                self._reference_vol = \
                    np.pi * np.trapz((p.depth(x) - p.horizon) * abs(x), x=x)
            else:
                self._reference_vol = 0.
        return self._reference_vol

    @reference_vol.deleter
    def reference_vol(self):
        self._reference_vol = None

    @property
    def ref_vol_change(self):
        if self._ref_vol_change is None:
            self._ref_vol_change = self.reference_vol
            if self.parent is not None:
                self._ref_vol_change -= self.parent.reference_vol
        return self._ref_vol_change

    @property
    def radius(self) -> float:
        """:type: float"""
        if self._radius is None:
            try:
                self._radius = self.crater.radius
            except AttributeError:
                self._radius = np.nan
        return self._radius

    @radius.deleter
    def radius(self):
        self._radius = None
        del self.radius_change, self.sigma_radius

    @radius.setter
    def radius(self, val):
        if self.crater is not None:
            raise AttributeError("Radius already given: %s m" % self.radius)
        else:
            self._radius = val
            self._radius_change = None

    @property
    def sigma_radius(self) -> float:
        if self._sigma_radius is None:
            if self.crater is not None:
                self._sigma_radius = self.crater.sigma_radius
            else:
                self._sigma_radius = crat.sd_length(self.radius)
        return self._sigma_radius

    @sigma_radius.deleter
    def sigma_radius(self):
        self._sigma_radius = None
        del self.sigma_radius_change

    @property
    def radius_change(self) -> float:
        """:rtype: float"""
        if self._radius_change is None:
            if self.parent is None:
                self._radius_change = self.radius
            else:
                self._radius_change = self.radius - self.parent.radius
        return self._radius_change

    @radius_change.deleter
    def radius_change(self):
        self._radius_change = None

    @property
    def sigma_radius_change(self) -> float:
        if self._sigma_radius_change is None:
            self._sigma_radius_change = self.sigma_radius ** 2
            if self.parent is not None:
                self._sigma_radius_change = sqrt(self._sigma_radius ** 2 +
                                                 self.parent.sigma_radius ** 2)
            else:
                self._sigma_radius_change = self.sigma_radius
        return self._sigma_radius_change

    @sigma_radius_change.deleter
    def sigma_radius_change(self):
        self._sigma_radius_change = None

    @property
    def r0(self) -> float:
        """
        :return: Radius parameter of control shape. This is a fit to the scaled
         crater radii at zero topography.
        :rtype: float
        """
        # TODO: ?add a flat bed radius object to Blast?
        if self._r0 is None:
            self._r0 = default_fbr.model(self.dob / self.energy ** ot) * \
                self.energy ** ot
        return self._r0

    @r0.deleter
    def r0(self):
        self._r0 = None
        del self.control_vol, self.f0, self.u0

    @property
    def sigma_r0(self) -> float:
        if self._sigma_r0 is None:
            self._sigma_r0 = default_fbr.standard_deviation(
                dbar=self.dob / self.energy ** ot,
                sd_dbar=self.sigma_dob / self.energy ** ot) * self.energy ** ot
        return self._sigma_r0

    @sigma_r0.deleter
    def sigma_r0(self) -> None:
        self._sigma_r0 = None

    @property
    def chloc(self) -> float:
        """
        :return: Charge location in the blasts coordinate system
        :rtype: float
        """
        if self.parent_crater is not None:
            p = self.parent_crater.profile
        else:
            p = self.crater.profile
        return p.center_depth - self.dob

    @property
    def effloc(self):
        """
        :return: Effective charge location in blasts coordinate system
        :rtype: float
        """
        if self.parent_crater is not None:
            p = self.parent_crater.profile
        else:
            p = self.crater.profile
        return p.center_depth - self.deff

    @property
    def plotstring(self):
        return "%s, %s" % (self.pad, self.run)

    def plot(self, ax):
        """Plot the parent (if any), and current crater surfaces, charge
        location and effective depth with respective levels into the given
        :class:`matplotlib.axes`.

        :param ax: An axes object to plot into.
        :type ax: :class:`matplotlib.axes.AxesSubplot`

        :returns: The ax argument.
        :rtype: :class:`matplotlib.axes.AxesSubplot`
        """
        # colors
        gr0, g0, red0, b0 = '#c5c5c5', '#25a525', '#a52525', '#2525b5'
        fs = 'medium'  # font size

        p = self.crater.profile.para
        if self.parent_crater is not None:
            try:
                pp = self.parent_crater.profile.para
            except AttributeError:
                pp = self.parent_crater.profile
        else:
            pp = p

        h = pp.horizon  # the horizon
        chloc = pp.center_depth - self.dob    # charge location
        effloc = pp.center_depth - self.deff  # effective depth
        z0 = self.z0  # control shape amplitude
        # control shape
        csx = np.linspace(pp.hrz[0], pp.hrz[-1], 500, True)
        csz = self.zc(csx)

        # plot
        # ====
        # basic setup
        ax.set_aspect(1.0)
        ax.set_ylim(-3, .5)
        ax.set_xlim(-2., 2.)
        ax.set_xlabel(r'$r\ /\ \mathrm{m}$')
        ax.set_ylabel(r'$z\ /\ \mathrm{m}$')
        # profile lines
        ax.plot(pp.hrz, pp.vrt)
        ax.plot(p.hrz, p.vrt, color=g0, lw=1, ls='dashed')
        # dotted horizon
        ax.plot([-2., 2.], 2 * [h], color='k', ls='dotted')
        # amplitude line
        ax.plot([-2., 0.], 2 * [z0 + h], color='k', ls='dotted')
        # the control shape
        ax.plot(csx, csz, color=red0)
        if self.parent_crater is not None:
            ax.fill_between(csx, pp.depth(csx), csz, color=gr0)
        else:
            ax.fill_between(csx, h * np.ones(len(csx)), csz, color=gr0)
        # marker of charge location
        ax.plot([0.], [chloc], color=red0, marker='x', markersize=10)
        ax.plot([0.], [chloc], color=red0, marker='o', markersize=5)
        # marker of effective depth
        ax.plot([0.], [effloc], color=red0, marker='o', markersize=5)
        # annotations
        # ===========
        # crater radius r0
        ap = dict(arrowstyle='<->', color=g0)
        ax.annotate('', (p.rim_1[0], -.12), xytext=(p.rim_2[0], -.12),
                    color=g0, fontsize=fs, arrowprops=ap)
        ax.text(0., -.1, r'$2r_0$', color=g0, va='bottom', ha='center')
        # crater bottom
        r = -1.5
        ax.plot([r, 0], 2 * [pp.center_depth], ls='dotted', color=b0)
        # depth of burial
        ap = dict(arrowstyle='<->', color='black')
        r = -1.25
        ax.plot([r, 0], 2 * [chloc], ls='dotted')
        ax.annotate('', (r, pp.center_depth), xytext=(r, chloc),
                    color='black', fontsize=fs, arrowprops=ap)
        ax.text(r - .03, .5 * (pp.center_depth + chloc), r'$d$', color='black',
                va='center', ha='right')
        # effective depth
        ap = dict(arrowstyle='<->', color='black')
        r = -1.5
        ax.plot([r, 0], 2 * [effloc], ls='dotted', color='black')
        ax.annotate('', (r, pp.center_depth), xytext=(r, effloc),
                    color='black', fontsize=fs, arrowprops=ap)
        ax.text(r - .02, .5 * (pp.center_depth + effloc), r'$d_{eff}$',
                color='black', va='center', ha='right')

    def __str__(self):
        s = "pad %s, blast %s, parent: %s\n" \
            % (self.pad, self.run, self.parent.__repr__())
        s += "energy this blasts, cumulative: %.4f, %.4f\n" \
             % (self.energy, self.energy_cumul)
        s += "dob: %4f\n" % self.dob
        return s

    def __repr__(self):
        s = "Blast("
        if self.desc != "":
            s += "%s, " % self.desc
        s += "pad=%s, run=%s)" % (self.pad, self.run)
        return s
