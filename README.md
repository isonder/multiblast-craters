This is a simple helper for the interpretation of (volcanic) crater morphologies, and some phenomena that we associate with their formation.

This version of the code was used for the analysis of the data as published in a JGR article on the energy-scaling on craters.

I. Sonder; A. H. Graettinger & G. A. Valentine, 2015: *Scaling multiblast craters: general approach and application to volcanic craters.* Journal of Geophysical Research: Solid Earth, [doi: 10.1002/2015JB012018.](https://doi.org/10.1002/2015JB012018)
